
var app = angular.module("pokedexApp", ["ngRoute"]);
app.config(function($routeProvider) {
    $routeProvider
    .when("/", {
        templateUrl : "./templates/list.html",
        controller : "PokeListController"
    })
    .when("/pokemon/:pokeID", {
        templateUrl : "./templates/pokemon.html",
        controller : "PokemonController"
    })
    ;
});

app.controller('PokeListController', function($scope, $http) {

    $http.get('/gabe/pokedex/rest/pokelist').then(function(response) {
        $scope.allPokemonz = response.data;

        console.log(response.data);


        jQuery('.inner').css('opacity', '1');
        jQuery('.loader').hide();
    });




});

app.controller('PokemonController', function($scope, $routeParams, $http, $q) {
    $scope.pid = $routeParams.pokeID;

    //Format zeros
    num = $routeParams.pokeID;
    size = "3";
    var s = num + "";
    while (s.length < size) s = "0" + s;
    $scope.formattedPID = s;

    $http.get('/gabe/pokedex/rest/pokemon/' +$routeParams.pokeID ).then(function(response) {
        $scope.pokeObject = response.data;

        console.log(response.data);

        jQuery('.inner').css('opacity', '1');
        jQuery('.loader').hide();

    });

    // var promise1 = $http.get('http://pokeapi.co/api/v2/pokemon/' +$routeParams.pokeID ).then(function(response) {
    //     $scope.pokeObject = response.data;
    //
    //     console.log(response.data);
    //
    // });
    //
    // var promise2 = $http.get('http://pokeapi.co/api/v2/pokemon-species/' +$routeParams.pokeID ).then(function(response) {
    //     $scope.pokeObject2 = response.data;
    //
    //     var generationHolder = response.data.generation.name;
    //     var fixedGeneration = generationHolder.replace(/generation-/i, '');
    //     $scope.generation = fixedGeneration.toUpperCase();
    //
    //     $scope.hatchCycles = response.data.hatch_counter;
    //     $scope.hatchSteps = (response.data.hatch_counter * 255);
    //
    //     console.log(response.data);
    // });
    //
    // $q.all([promise1, promise2]).then(function(data){
    // 	//console.log(data[0], data[1]);
    //     jQuery('.inner').css('opacity', '1');
    //     jQuery('.loader').hide();
    // });

});
